<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::group(['prefix' => 'apiutb'], function () {
    Route::get('empleados', 'EmpleadoController@index');
    Route::post('empleados', 'EmpleadoController@store');
    Route::get('empleados/{empleados}', 'EmpleadoController@show');
    Route::put('empleados/{empleados}', 'EmpleadoController@update');
    Route::delete('empleados/{empleados}', 'EmpleadoController@destroy');
});

Route::get('/{any?}', 'HomeController@index')->name('home')->where('any', '.*');
