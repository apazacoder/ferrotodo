import Vuetify from 'vuetify';
import Vue from 'vue'; // window.Vue = require('vue');
import VueRouter from 'vue-router';

Vue.use(VueRouter);

// componentes
Vue.component('panel-component', require('./components/PanelComponent').default);

// vistas
import Empleados from './views/Empleados';
import Marcas from './views/Marcas';
import Stock from './views/Stock';
import Promociones from './views/Promociones';
import Clientes from './views/Clientes';
import Ventas from './views/Ventas';
import Reportes from './views/Reportes';
import Reportedeventasdeclientes from './views/Reportedeventasdeclientes';
import Reportedeventasdeproductos from './views/Reportedeventasdeproductos';
import Reportedeventaspormes from './views/Reportedeventaspormes';
import Reportedeventasporstock from './views/Reportedeventasporstock';


import Noencontrado from './views/Noencontrado';



require('./bootstrap');
const vuetifyOpts = {};
Vue.use(Vuetify);


const router = new VueRouter({
    routes: [
        {
            path: '/empleados', name: 'empleados', component: Empleados,
            meta: {breadcrumb: "Empleados",},
        },
        {
            path: '/marcas', name: 'marcas', component: Marcas,
            meta: {breadcrumb: "Marcas",}
        },
        {
            path: '/stock', name: 'stock', component: Stock,
            meta: {breadcrumb: "Stock",}
        },
        {
            path: '/promociones', name: 'promociones', component: Promociones,
            meta: {breadcrumb: "Promociones",}
        },
        {
            path: '/clientes', name: 'clientes', component: Clientes,
            meta: {breadcrumb: "Clientes",}
        },
        {
            path: '/ventas', name: 'ventas', component: Ventas,
            meta: {breadcrumb: "Ventas",}
        },
        {
            path: '/reportes', name: 'reportes', component: Reportes,
            meta: {breadcrumb: "Reportes",}
        },
        {
            path: '/reportedeventasdeclientes', name: 'reportedeventasdeclientes', component: Reportedeventasdeclientes,
            meta: {breadcrumb: "Reportedeventasdeclientes",}
        },
        {
            path: '/reportedeventasdeproductos', name: 'reportedeventasdeproductos', component: Reportedeventasdeproductos,
            meta: {breadcrumb: "Reportedeventasdeproductos",}
        },
        {
            path: '/reportedeventaspormes', name: 'reportedeventaspormes', component: Reportedeventaspormes,
            meta: {breadcrumb: "Reportedeventaspormes",}
        },
        {
            path: '/reportedeventasporstock', name: 'reportedeventasporstock', component: Reportedeventasporstock,
            meta: {breadcrumb: "Reportedeventasporstock",}
        },
        {
            path: '*', name: 'noencontrado', component: Noencontrado,
            meta: {breadcrumb: "Página no encontrado",}
        },
    ],
    mode: 'history',
    scrollBehavior() {
        return {x: 0, y: 0}
    }
});

const app = new Vue({
    vuetify: new Vuetify(vuetifyOpts),
    el: '#app',
    data() {
        return {
            emailsNumber: 2,
        }
    },
    router,
    methods: {
        addEmail() {
            console.log("Trying to add an email");
            this.emailsNumber += 1;
        }
    }

});
