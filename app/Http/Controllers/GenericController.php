<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;

class GenericController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected $model;
    protected $resourceName;
    protected $singularName;

    public function index() {
        $items = $this->model::get();
        return response()->json([
            $this->resourceName => $items,
            'message' => ($items)? 'Mostrando items' : ''
        ]);
    }

    // saving
    public function store(Request $request) {
        $done = true;
        try {
            $this->model::create($request->all());
        } catch (\Exception $e) {
            $done = false;
        }
        return response()->json([
            'message' => ($done) ? 'Datos del'.$this->singularName.'guardados' : ''
        ]);
    }

    // show a resources
    public function show($id) {
        $this->resourceName = $this->model::find($id);

        return response()->json([
            $this->resourceName => ($this->resourceName) ? $this->resourceName : 'El '.$this->singularName.' no existe',
        ]);
    }

    // update a resource
    public function update(Request $request, $id) {
        $done = true;
        try {
            $item = $this->model::find($id);
            $item->update($request->all());
        } catch (\Exception $e) {
            $done = false;
        }
        return response()->json([
            'message' => ($done) ? 'Datos del '.$this->singularName.' actualizados' : ''
        ]);
    }

    // destroy a resources
    public function destroy($id) {
        $done = true;
        try {
            $item = $this->model::find($id);
            $item->delete();
        } catch (\Exception $e) {
            $done = false;
        }
        return response()->json([
            'message' => ($done) ? 'Datos del '.$this->singularName.' borrados' : ''
        ]);
    }

}
