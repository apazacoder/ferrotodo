<?php

namespace App\Http\Controllers;

use App\Empleado;
use Illuminate\Http\Request;

class EmpleadoController extends GenericController
{
    public function __construct(){
        $this->model = new Empleado();
        $this->resourceName = "empleados";
        $this->singularName = "empleado";
    }
}
